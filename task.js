const aTasks = [];

function onTaskAdd() {
  let oTaskInput = document.getElementById("task-input");
  aTasks.push({task: oTaskInput.value, state: 'to-do'});
  oTaskInput.value = '';
  renderToDoList();
}

function renderToDoList() {
  let oToDoList = document.getElementById('to-do-list');
  oToDoList.innerHTML = '';
  for(let i=0; i < aTasks.length; i++) {
    let checkBox = document.createElement('input');
    checkBox.type = 'checkbox';
    checkBox.classList = 'mr-10';
    let oLi = document.createElement('li');
    oLi.appendChild(checkBox);
    oLi.append(aTasks[i].task);
    oToDoList.appendChild(oLi);
  }
}
